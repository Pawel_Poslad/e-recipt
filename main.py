import tkinter as tk
from tkinter import *
import sqlite3
from reportlab.pdfgen import canvas
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase import pdfmetrics
from reportlab.lib import colors
from reportlab.lib.units import inch
import re

LARGE_FONT = ("Verdana", 12)

class Recipt(tk.Tk):

    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand=True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (StartPage, New_Patient, New_Medicine, Choose_Patient):
            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label0 = tk.Label(self, text="E-Recepta", font=LARGE_FONT)
        label0.pack()

        button0 = tk.Button(self, text="Wprowadź Pacjenta",
                           command=lambda: controller.show_frame(New_Patient))
        button0.pack(side="top", fill="x")

        button1 = tk.Button(self, text="Wprowadź Lek",
                           command=lambda: controller.show_frame(New_Medicine))
        button1.pack(side="top", fill="x")

        button2 = tk.Button(self, text="Utwórz Receptę",
                            command=lambda: controller.show_frame(Choose_Patient))
        button2.pack(side="top", fill="x")



class New_Patient(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label0 = tk.Label(self, text="Wprowadź dane nowego pacjenta", font=LARGE_FONT)
        label0.pack()

        button0 = tk.Button(self, text="Wróć",
                            command=lambda: controller.show_frame(StartPage))
        button0.pack(side="bottom", fill="x")

        def save_patient():

            first_name = entry0.get()
            print("Imie: " + first_name)
            entry0.delete(0, tk.END)

            last_name = entry1.get()
            print("Nazwisko: " + last_name)
            entry1.delete(0, tk.END)

            pesel = entry2.get()
            print("Pesel: " + pesel)
            entry2.delete(0, tk.END)

            street = entry3.get()
            print("Ulica: " + street)
            entry3.delete(0, tk.END)

            city = entry4.get()
            print("Miasto: " + city)
            entry4.delete(0, tk.END)

            post_code = entry5.get()
            print("Kod Pocztowy: " + post_code)
            entry5.delete(0, tk.END)

            try:
                sqliteConnection = sqlite3.connect('SQLite_Python.db')
                sqlite_create_table_query = '''CREATE TABLE patient (
                                    patient_id INTEGER PRIMARY KEY,
                                    first_name TEXT NOT NULL,
                                    last_name TEXT NOT NULL,
                                    pesel TEXT NOT NULL UNIQUE,
                                    street TEXT NOT NULL,
                                    city TEXT NOT NULL,
                                    post_code TEXT NOT NULL
                                    );'''

                cursor = sqliteConnection.cursor()
                print("Successfully Connected to SQLite")
                cursor.execute(sqlite_create_table_query)
                sqliteConnection.commit()
                print("SQLite table created")

                cursor.close()

            except sqlite3.Error as error:
                print("Error while creating a sqlite table", error)
            finally:
                if (sqliteConnection):
                    sqliteConnection.close()
                    print("sqlite connection is closed")

            try:
                sqliteConnection = sqlite3.connect('SQLite_Python.db')
                cursor = sqliteConnection.cursor()
                print("Connected to SQLite")

                sqlite_insert_with_param = """INSERT INTO `patient`
                                      ('first_name', 'last_name', 'pesel', 'street', 'city', 'post_code') 
                                       VALUES (?, ?, ?, ?, ?, ?);"""

                save_data = (first_name, last_name, pesel, street, city, post_code)
                cursor.execute(sqlite_insert_with_param, save_data)
                sqliteConnection.commit()
                print("Python Variables inserted successfully into SqliteDb_developers table")

                cursor.close()

            except sqlite3.Error as error:
                print("Failed to insert Python variable into sqlite table", error)
            finally:
                if (sqliteConnection):
                    sqliteConnection.close()
                    print("The SQLite connection is closed")

        button1 = tk.Button(self, text="Zapisz", command=save_patient)
        button1.pack(side="bottom", fill="x")

        label0 = tk.Label(self, text="Imie:")
        label0.pack(side="left", fill="x", pady=10)
        label0.place(x=0, y=25, in_=self)
        entry0 = tk.Entry(self)
        entry0.pack(side="left", fill="x", pady=10)
        entry0.place(x=85, y=25, in_=self)

        label1 = tk.Label(self, text="Nazwisko:")
        label1.pack(side="left", fill="x", pady=10)
        label1.place(x=0, y=50, in_=self)
        entry1 = tk.Entry(self)
        entry1.pack(side="left", fill="x", pady=10)
        entry1.place(x=85, y=50, in_=self)

        label2 = tk.Label(self, text="Pesel:")
        label2.pack(side="left", fill="x", pady=10)
        label2.place(x=0, y=75, in_=self)
        entry2 = tk.Entry(self)
        entry2.pack(side="left", fill="x", pady=10)
        entry2.place(x=85, y=75, in_=self)

        label3 = tk.Label(self, text="Ulica:")
        label3.pack(side="left", fill="x", pady=10)
        label3.place(x=0, y=100, in_=self)
        entry3 = tk.Entry(self)
        entry3.pack(side="left", fill="x", pady=10)
        entry3.place(x=85, y=100, in_=self)

        label4 = tk.Label(self, text="Miasto:")
        label4.pack(side="left", fill="x", pady=10)
        label4.place(x=0, y=125, in_=self)
        entry4 = tk.Entry(self)
        entry4.pack(side="left", fill="x", pady=10)
        entry4.place(x=85, y=125, in_=self)

        label5 = tk.Label(self, text="Kod Pocztowy:")
        label5.pack(side="left", fill="x", pady=10)
        label5.place(x=0, y=150, in_=self)
        entry5 = tk.Entry(self)
        entry5.pack(side="left", fill="x", pady=10)
        entry5.place(x=85, y=150, in_=self)

class New_Medicine(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label0 = tk.Label(self, text="Wprowadź nowy lek", font=LARGE_FONT)
        label0.pack()

        def save_medicine():

            medicine_name = entry0.get()
            print("Nazwa Leku: " + medicine_name)
            entry0.delete(0, tk.END)

            portion = entry1.get()
            print("Dawka: " + portion)
            entry1.delete(0, tk.END)

            medicine_form = entry2.get()
            print("Postać: " + medicine_form)
            entry2.delete(0, tk.END)

            amount_medicine = entry3.get()
            print("Ilość: " + amount_medicine)
            entry3.delete(0, tk.END)

            dosage_method = entry4.get()
            print("Sposób dawkowania: " + dosage_method)
            entry4.delete(0, tk.END)

            try:
                sqliteConnection = sqlite3.connect('SQLite_Python.db')
                sqlite_create_table_query = '''CREATE TABLE medicine (
                                    medicine_id INTEGER PRIMARY KEY,
                                    medicine_name TEXT NOT NULL,
                                    portion TEXT NOT NULL,
                                    medicine_form TEXT NOT NULL,
                                    amount_medicine TEXT NOT NULL,
                                    dosage_method TEXT NOT NULL
                                    );'''

                cursor = sqliteConnection.cursor()
                print("Successfully Connected to SQLite")
                cursor.execute(sqlite_create_table_query)
                sqliteConnection.commit()
                print("SQLite table created")

                cursor.close()

            except sqlite3.Error as error:
                print("Error while creating a sqlite table", error)
            finally:
                if (sqliteConnection):
                    sqliteConnection.close()
                    print("sqlite connection is closed")

            try:
                sqliteConnection = sqlite3.connect('SQLite_Python.db')
                cursor = sqliteConnection.cursor()
                print("Connected to SQLite")

                sqlite_insert_with_param = """INSERT INTO `medicine`
                                      ('medicine_name', 'portion', 'medicine_form', 'amount_medicine', 'dosage_method') 
                                       VALUES (?, ?, ?, ?, ?);"""

                save_data = (medicine_name, portion, medicine_form, amount_medicine, dosage_method)
                cursor.execute(sqlite_insert_with_param, save_data)
                sqliteConnection.commit()
                print("Python Variables inserted successfully into SqliteDb_developers table")

                cursor.close()

            except sqlite3.Error as error:
                print("Failed to insert Python variable into sqlite table", error)
            finally:
                if (sqliteConnection):
                    sqliteConnection.close()
                    print("The SQLite connection is closed")

        label0 = tk.Label(self, text="Nazwa Leku:")
        label0.pack(side="left", fill="x", pady=10)
        label0.place(x=0, y=25, in_=self)
        entry0 = tk.Entry(self)
        entry0.pack(side="left", fill="x", pady=10)
        entry0.place(x=120, y=25, in_=self)

        label1 = tk.Label(self, text="Dawka:")
        label1.pack(side="left", fill="x", pady=10)
        label1.place(x=0, y=50, in_=self)
        entry1 = tk.Entry(self)
        entry1.pack(side="left", fill="x", pady=10)
        entry1.place(x=120, y=50, in_=self)

        label2 = tk.Label(self, text="Postać:")
        label2.pack(side="left", fill="x", pady=10)
        label2.place(x=0, y=75, in_=self)
        entry2 = tk.Entry(self)
        entry2.pack(side="left", fill="x", pady=10)
        entry2.place(x=120, y=75, in_=self)

        label3 = tk.Label(self, text="Ilość:")
        label3.pack(side="left", fill="x", pady=10)
        label3.place(x=0, y=100, in_=self)
        entry3 = tk.Entry(self)
        entry3.pack(side="left", fill="x", pady=10)
        entry3.place(x=120, y=100, in_=self)

        label4 = tk.Label(self, text="Sposób dawkowania:")
        label4.pack(side="left", fill="x", pady=10)
        label4.place(x=0, y=125, in_=self)
        entry4 = tk.Entry(self)
        entry4.pack(side="left", fill="x", pady=10)
        entry4.place(x=120, y=125, in_=self)

        button0 = tk.Button(self, text="Wróć",
                            command=lambda: controller.show_frame(StartPage))
        button0.pack(side="bottom", fill="x")

        button1 = tk.Button(self, text="Zapisz", command=save_medicine)
        button1.pack(side="bottom", fill="x")

class Choose_Patient(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label0 = tk.Label(self, text="Wybór pacjenta", font=LARGE_FONT)
        label0.pack()

        button0 = tk.Button(self, text="Wróć",
                            command=lambda: controller.show_frame(StartPage))
        button0.pack(side="bottom", fill="x")

        def Print():

            patient_string = str((list0.get(ANCHOR)))
            print(patient_string)
            cutted_patient = patient_string.split(",")

            cutted_medicine = str((list1.get(ANCHOR)))
            print(cutted_medicine)
            cutted_medicine = cutted_medicine.split(",")

            fileName = 'Recipt.pdf'
            pdf = canvas.Canvas(fileName)
            pdf.setPageSize((300, 630))
            # TopLine
            pdf.line(20, 610, 280, 610)

            # RightLine
            pdf.line(20, 610, 20, 20)

            # BottomLine
            pdf.line(20, 20, 280, 20)

            # LeftLine
            pdf.line(280, 20, 280, 610)

            # ReceptaArea
            pdf.line(20, 504, 280, 504)
            recepta = pdf.beginText()
            recepta.setTextOrigin(23, 597)
            recepta.textLine(text='Recepta')
            pdf.drawText(recepta)

            # PacjentArea
            pdf.line(20, 386, 280, 386)
            pacjent = pdf.beginText()
            pacjent.setTextOrigin(23, 493)
            pacjent.textLine(text='Pacjent:')
            pdf.drawText(pacjent)

            patient_full_name = pdf.beginText()
            patient_full_name.setTextOrigin(43, 468)
            patient_full_name.textLine(text=cutted_patient[0])
            pdf.drawText(patient_full_name)

            patient_street = pdf.beginText()
            patient_street.setTextOrigin(43, 448)
            patient_street.textLine(text=cutted_patient[2])
            pdf.drawText(patient_street)

            patient_city = pdf.beginText()
            patient_city.setTextOrigin(43, 428)
            patient_city.textLine(text=cutted_patient[3])
            pdf.drawText(patient_city)

            PESEL = pdf.beginText()
            PESEL.setTextOrigin(23, 390)
            PESEL.textLine(text='PESEL: ' + cutted_patient[1])
            pdf.drawText(PESEL)

            #Lek_Area
            medicine_topic = pdf.beginText()
            medicine_topic.setTextOrigin(23, 370)
            medicine_topic.textLine(text='Lek')
            pdf.drawText(medicine_topic)

            medicine_name = pdf.beginText()
            medicine_name.setTextOrigin(43, 355)
            medicine_name.textLine(text=cutted_medicine[0] + " " + cutted_medicine[1])
            pdf.drawText(medicine_name)

            medicine_form = pdf.beginText()
            medicine_form.setTextOrigin(43, 340)
            medicine_form.textLine(text=cutted_medicine[2] + " " + cutted_medicine[3])
            pdf.drawText(medicine_form)

            dosage_method = pdf.beginText()
            dosage_method.setTextOrigin(43, 325)
            dosage_method.textLine(text=cutted_medicine[4])
            pdf.drawText(dosage_method)

            # Data_WystawieniaArea
            pdf.line(20, 126, 280, 126)
            data_wystawienia = pdf.beginText()
            data_wystawienia.setTextOrigin(23, 113)
            data_wystawienia.textLine(text='Data wystawienia:')
            pdf.drawText(data_wystawienia)

            # Data_RealizacjiArea
            pdf.line(20, 73, 150, 73)
            data_realizacji = pdf.beginText()
            data_realizacji.setTextOrigin(23, 60)
            data_realizacji.textLine(text='Data Realizacji od dnia:')
            pdf.drawText(data_realizacji)

            # Podpis_LekarzaArea
            pdf.line(150, 20, 150, 126)
            lekarz = pdf.beginText()
            lekarz.setTextOrigin(153, 113)
            lekarz.textLine(text='Dane i podpis lekarza')
            pdf.drawText(lekarz)

            pdf.save()

        button1 = tk.Button(self, text="Drukuj",
                            command=lambda: Print())
        button1.pack(side="bottom", fill="x")

        def View_DB_patient():
            try:
                sqliteConnection = sqlite3.connect('SQLite_Python.db')
                cursor = sqliteConnection.cursor()
                print("Connected to SQLite")

                sqlite_select_query = """SELECT * from patient"""
                cursor.execute(sqlite_select_query)
                records = cursor.fetchall()
                print("Total rows are:  ", len(records))
                print("Printing each row")
                print(records)
                list0.delete(0, tk.END)
                for row in records:
                    line = (str(row[1]) + " " + str(row[2]) + "," + str(row[3]) + "," + str(row[4]) + "," + str(row[6]) + " " + str(row[5]))
                    list0.insert(tk.END, line)

                cursor.close()

            except sqlite3.Error as error:
                print("Failed to read data from sqlite table", error)
            finally:
                if (sqliteConnection):
                    sqliteConnection.close()
                    print("The SQLite connection is closed")

        def View_DB_medicine():
            try:
                sqliteConnection = sqlite3.connect('SQLite_Python.db')
                cursor = sqliteConnection.cursor()
                print("Connected to SQLite")

                sqlite_select_query = """SELECT * from medicine"""
                cursor.execute(sqlite_select_query)
                records = cursor.fetchall()
                print("Total rows are:  ", len(records))
                print("Printing each row")
                print(records)
                list1.delete(0, tk.END)
                for row in records:
                    line = (str(row[1]) + "," + str(row[2]) + "," + str(row[3]) + "," + str(row[4]) + "," + str(row[5]))
                    list1.insert(tk.END, line)

                cursor.close()

            except sqlite3.Error as error:
                print("Failed to read data from sqlite table", error)
            finally:
                if (sqliteConnection):
                    sqliteConnection.close()
                    print("The SQLite connection is closed")

        def delete_patient():

            marked = str((list0.get(ANCHOR)))
            print(marked)
            cutted = marked.split(",")
            pesel = cutted[1]

            try:
                sqliteConnection = sqlite3.connect('SQLite_Python.db')
                cursor = sqliteConnection.cursor()
                print("Connected to SQLite")

                sql_update_query = """DELETE from patient where pesel = ?"""
                cursor.execute(sql_update_query, (pesel,))
                sqliteConnection.commit()
                print("Record deleted successfully")

                cursor.close()

            except sqlite3.Error as error:
                print("Failed to delete reocord from a sqlite table", error)
            finally:
                if (sqliteConnection):
                    sqliteConnection.close()
                    print("sqlite connection is closed")

        def delete_medicine():

            marked = str((list1.get(ANCHOR)))
            print(marked)
            cutted = marked.split(",")
            medicine_name = cutted[1]
            amount_medicine = cutted[4]

            try:
                sqliteConnection = sqlite3.connect('SQLite_Python.db')
                cursor = sqliteConnection.cursor()
                print("Connected to SQLite")

                sql_update_query = """DELETE from medicine where medicine_name, amount_medicine = ?, ?"""
                cursor.execute(sql_update_query, (medicine_name, amount_medicine))
                sqliteConnection.commit()
                print("Record deleted successfully")

                cursor.close()

            except sqlite3.Error as error:
                print("Failed to delete reocord from a sqlite table", error)
            finally:
                if (sqliteConnection):
                    sqliteConnection.close()
                    print("sqlite connection is closed")

        button2 = tk.Button(self, text="Usuń lek",
                            command=delete_medicine)
        button2.pack(side="bottom", fill="x")

        button3 = tk.Button(self, text="Usuń pacjenta",
                            command=delete_patient)
        button3.pack(side="bottom", fill="x")

        button4 = tk.Button(self, text="Pokaż dostępne leki",
                            command=View_DB_medicine)
        button4.pack(side="bottom", fill="x")

        button5 = tk.Button(self, text="Pokaż dostępnych pacjentów",
                            command=View_DB_patient)
        button5.pack(side="bottom", fill="x")


        sb1 = tk.Scrollbar(self,)
        sb1.pack(side="right", fill="y")

        sb0 = tk.Scrollbar(self,)
        sb0.pack(side="right", fill="y")

        list0 = tk.Listbox(self, height=5, width=50)
        list0.pack(side="top", fill="both")

        list0.configure(yscrollcommand=sb0.set)
        sb0.configure(command=list0.yview)

        list1 = tk.Listbox(self, height=5, width=50)
        list1.pack(side="top", fill="both")

        list1.configure(yscrollcommand=sb1.set)
        sb1.configure(command=list1.yview)

app = Recipt()
app.geometry('{}x{}'.format(600, 350))
app.mainloop()

